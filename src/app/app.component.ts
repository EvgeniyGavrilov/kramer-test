import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Monitor } from './interfaces/monitor';
import { MonitorsService } from './services/monitors.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'test';
  monitors: Monitor[] = [];
  filterByName: string = '';
  monitorName: string = '';
  unsubscribe$: Subject<null> = new Subject();


  constructor(private monitorsService: MonitorsService) {}

  ngOnInit(): void {
    this.monitorsService.getMonitorsInfo().pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((monitors) => {
      this.monitors = monitors;
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(null);
    this.unsubscribe$.complete();
  }

  setActiveMonitor(monitorName: string) {
    this.monitorName === monitorName
      ? (this.monitorName = '')
      : (this.monitorName = monitorName);
  }
}
