import { Pipe, PipeTransform } from '@angular/core';
import { Monitor } from '../interfaces/monitor';

@Pipe({
  name: 'monitorFilter'
})
export class MonitorFilterPipe implements PipeTransform {

  transform(monitors: Monitor[], filterBy: string): Monitor[] {
    return monitors.filter(monitor => monitor.Name.toLowerCase().includes(filterBy.toLowerCase())) ;
  }

}
