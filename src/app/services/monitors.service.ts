import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { Monitor } from '../interfaces/monitor';




@Injectable({
  providedIn: 'root'
})
export class MonitorsService {
  fileNames: string[] = [
    "BenQ_SC3211", 
    "Dell_ZT60", 
    "Haier_LE39B50", 
    "LG_50LA621Y", 
    "Mag_RD24L", 
    "Normande_ND3276", 
    "Panasonic_TH-L32B6", 
    "Philips_55PFL6008", 
    "Philips_226V4LSB", 
    "Samsung_UA46F6400", 
    "Sharp_LC50LE450M",
    "Samsung_UA55F6400", 
    "Sony_KDL50W656"
  ];
  basePath = '../../assets/JSONmonitors/';
  prefix = '.json';
  requestUrlArr: Observable<Monitor>[] = []

  constructor(private http: HttpClient) {
    this.getURLs(this.fileNames, this.basePath, this.prefix)
  }

  getMonitorsInfo(): Observable<Monitor[]> {
    return forkJoin([...this.requestUrlArr]);
  };

  getURLs(fileNames: string[], basePath: string, prefix: string) {
    fileNames.forEach(monitorName => {
      this.requestUrlArr.push(this.http.get<Monitor>(basePath + monitorName + prefix));
    })
  }
}
